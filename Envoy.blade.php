@servers(['web' => 'devops@wahyutrip.floo.id'])

@setup
$repository = 'git@gitlab.com:wahyutrip/flooid-binusmaya.git';
    $releases_dir = '/var/www/app/releases';
    $app_dir = '/var/www/app';
    $release = 'release_'.date('YmdHis');
    $new_release_dir = $releases_dir .'/'. $release;
@endsetup

@story('deploy')
    clone_repository
    run_composer
    copy_env
    update_symlinks
    run_artisan
@endstory

@task('clone_repository')
    echo 'Cloning repository'
    [ -d {{ $releases_dir }} ] || mkdir {{ $releases_dir }}
    git clone --depth 1 {{ $repository }} {{ $new_release_dir }}
@endtask

@task('run_composer')
    echo "Starting deployment ({{ $release }})"
    cd {{ $releases_dir }}/{{ $release }}
    composer install --prefer-dist --no-scripts -q -o
    echo 'Finish deployment ({{ $release }})'
@endtask

@task('copy_env')
    echo "Copy .env"
    cd {{ $app_dir }}
    cp .env.example .env
    echo "Done"
@endtask

@task('update_symlinks')
    echo "Linking storage directory"
    rm -rf {{ $new_release_dir }}/storage
    ln -nfs {{ $app_dir }}/storage {{ $new_release_dir }}/storage

    echo 'Linking .env file'
    ln -nfs {{ $app_dir }}/.env {{ $new_release_dir }}/.env

    echo 'Linking current release'
    ln -nfs {{ $new_release_dir }} {{ $app_dir }}/current
@endtask

@task('run_artisan')
    echo "Move to ({{ $releases_dir }}/{{ $release }})"
    cd {{ $releases_dir }}/{{ $release }}
    echo "Running artisan"
    php artisan key:generate
    echo 'Done'
@endtask
